import Link from "next/link";
import { useRouter } from "next/router";

export default function Layout({ children, title }) {
  const router = useRouter();

  return (
    <>
      main nav
      <nav style={{ display: "flex", gap: 10 }}>
        <Link className={router.pathname == "/" ? "active" : ""} href="/">
          Home
        </Link>
        <Link className={router.pathname == "/contacts" ? "active" : ""} href="/contacts">
            Contacts
        </Link>
        <Link className={router.pathname == "/items" ? "active" : ""} href="/items">
          Items
        </Link>
      </nav>
      <h1>{title}</h1>
      <hr />
      <main>{children}</main>
      <hr />
      <footer style={{ color: "silver" }}>
        copyright xyz, 2022
        <br />
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by Yyy
        </a>
      </footer>
    </>
  );
}
