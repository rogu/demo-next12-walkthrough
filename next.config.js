module.exports = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'api.debugger.pl',
        port: '',
        pathname: '/assets/**',
      },
    ],
  }
};
