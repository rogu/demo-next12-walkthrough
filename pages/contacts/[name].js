import Layout from "../../components/layout";

function Contact({ contacts, contact }) {
  return (
    <Layout title="Server Side Rendering Example">
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {contacts.map((item) => (
          <a
            href={item.name}
            key={item.id}
            style={{ padding: 10, margin: 4, background: "yellow" }}
          >
            {item.name}
          </a>
        ))}
      </div>
      <br />
      <h1>{contact.name}</h1>
      <h2>{contact.phone}</h2>
      {/* {JSON.stringify(props)} */}
    </Layout>
  );
}

export default Contact;

async function getData(url) {
  const resp = await fetch(url);
  const { data } = await resp.json();
  return data;
}

export async function getServerSideProps({ params }) {
  const contacts = await getData("https://api.debugger.pl/workers");
  const contact = contacts.find((c) => c.name === params.name);
  return {
    props: {
      contacts,
      contact,
    },
  };
}
