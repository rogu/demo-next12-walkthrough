import Layout from "../../components/layout";
import Image from "next/image";

function Item({ data, item }) {
  return (
    <Layout title="Server Side Generate Example">
      post
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {data.map((item) => (
          <a
            href={item.title}
            key={item.id}
            style={{ padding: 10, margin: 4, background: "yellow" }}
          >
            {item.title}
          </a>
        ))}
      </div>
      <h2>{item.title}</h2>
      <br />
      <div style={{ display: "flex", gap: 20 }}>
        <Image src={item.imgSrc} alt="sdf" width={200} height={200} />
        <pre>{JSON.stringify(item, null, 2)}</pre>
      </div>
    </Layout>
  );
}

export default Item;

async function getData(url) {
  const resp = await fetch(url);
  const { data } = await resp.json();
  return data;
}

export async function getStaticPaths() {
  const data = await getData("https://api.debugger.pl/items");
  return {
    //paths: [{ params: { id: "1" } }, { params: { id: "2" } }],
    paths: data.map((item) => ({ params: { id: item.title } })),
    fallback: false, // false or 'blocking'
  };
}

export async function getStaticProps({ params }) {
  const data = await getData("https://api.debugger.pl/items");

  return {
    props: {
      data,
      item: data.find((item) => item.title === params.id),
    },
  };
}
