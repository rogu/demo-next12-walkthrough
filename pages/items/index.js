import Layout from "../../components/layout";

function Items({ data }) {
  return (
    <Layout title="Server Side Generate Example">
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {data.map((item) => (
          <a
            href={`items/${item.title}`}
            key={item.id}
            style={{ padding: 10, margin: 4, background: "yellow" }}
          >
            {item.title}
          </a>
        ))}
      </div>
    </Layout>
  );
}

export default Items;

async function getData(url) {
  const resp = await fetch(url);
  const { data } = await resp.json();
  return data;
}

export async function getStaticProps({ params }) {
  const data = await getData("https://api.debugger.pl/items");

  return {
    props: {
      data,
    },
  };
}
